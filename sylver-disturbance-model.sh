#! /usr/bin/env sh

# MIT License
# 
# Copyright (c) 2016  Federico Mari <mari@di.uniroma1.it>
#                     Toni Mancini <tmancini@di.uniroma1.it>
#                     Annalisa Massini <massini@di.uniroma1.it>
#                     Igor Melatti <melatti@di.uniroma1.it>
#                     Enrico Tronci <tronci@di.uniroma1.it>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

TICKS_PER_SECOND_DEFAULT=2
REMOVE_FAULT_TIME_DEFAULT=1
NUM_FAULTS_DEFAULT=4
MAX_NUM_OF_FAULTS_DEFAULT=2
MAX_NUM_OF_ACTIVE_FAULTS_DEFAULT=1
NUM_INPUTS_DEFAULT=1
MAX_NUM_OF_INPUTS_DEFAULT=1
INJECT_INPUT_PERIOD_DEFAULT=2.0
BEGIN_DISTURBANCE_WINDOW_DEFAULT=0.0

USAGE="Generates Disturbance Model for SyLVer in CMuphi procedural language.\n\n
Usage: `basename $0` [options]\n
Options:\n
\t\t[-h]\n
\t\t[-t ticks-per-second]\n
\t\t[-f number-of-faults]\n
\t\t[-g max-number-of-faults-in-trace]\n
\t\t[-e max-number-of-active-faults-in-same-moment]\n
\t\t[-r fault-duration-in-seconds]\n
\t\t[-i number-of-inputs]\n
\t\t[-j max-number-of-inputs-in-trace]\n
\t\t[-k seconds-multiple-of-which-inputs-can-be-injected]\n
\t\t[-l seconds-after-which-to-inject-disturbaces]\n
\n
Disturbances can be 1) faults or 2) input variations.\n
1) Faults are considered as temporary (in order not to destroy the system)\n
2) Inputs are permanent variations to the system.\n"


TICKS_PER_SECOND=$TICKS_PER_SECOND_DEFAULT
NUM_FAULTS=$NUM_FAULTS_DEFAULT
MAX_NUM_OF_FAULTS=$MAX_NUM_OF_FAULTS_DEFAULT
MAX_NUM_OF_ACTIVE_FAULTS=$MAX_NUM_OF_ACTIVE_FAULTS_DEFAULT
REMOVE_FAULT_TIME=$REMOVE_FAULT_TIME_DEFAULT
NUM_INPUTS=$NUM_INPUTS_DEFAULT
MAX_NUM_OF_INPUTS=$MAX_NUM_OF_INPUTS_DEFAULT
INJECT_INPUT_PERIOD=$INJECT_INPUT_PERIOD_DEFAULT
BEGIN_DISTURBANCE_WINDOW=$BEGIN_DISTURBANCE_WINDOW_DEFAULT

print_options() {
  echo "Ticks per second (-t arg) . . . . . . . . . . . . . . : $TICKS_PER_SECOND"
  echo "Total number of faults (-f arg) . . . . . . . . . . . : $NUM_FAULTS"
  echo "Max number of faults in a trace (-g arg). . . . . . . : $MAX_NUM_OF_FAULTS"
  echo "Max number of simultaneously active faults (-e arg) . : $MAX_NUM_OF_ACTIVE_FAULTS"
  echo "Fault duration in seconds (-r arg). . . . . . . . . . : $REMOVE_FAULT_TIME"
  echo "Total number of inputs (-i arg) . . . . . . . . . . . : $NUM_INPUTS"
  echo "Max number of inputs in a trace (-j arg). . . . . . . : $MAX_NUM_OF_INPUTS"
  echo "Inputs can be injected in seconds multiple of (-k arg): $INJECT_INPUT_PERIOD"
  echo "Disturbances can be injected after seconds (-l arg) . : $BEGIN_DISTURBANCE_WINDOW"
  echo
}

## Command line parsing
while getopts ht:f:g:e:r:i:j:k:l: OPT; do
  case "$OPT" in
    h)
      echo ""
      echo -e $USAGE
      echo ""
      print_options
      exit 0
      ;;
    t)
      TICKS_PER_SECOND=$OPTARG
      ;;
    f)
      NUM_FAULTS=$OPTARG
      ;;
    g)
      MAX_NUM_OF_FAULTS=$OPTARG
      ;;
    e)
      MAX_NUM_OF_ACTIVE_FAULTS=$OPTARG
      ;;
    r)
      REMOVE_FAULT_TIME=$OPTARG
      ;;
    i)
      NUM_INPUTS=$OPTARG
      ;;
    j)
      MAX_NUM_OF_INPUTS=$OPTARG
      ;;
    k)
      INJECT_INPUT_PERIOD=$OPTARG
      ;;
    l)
      BEGIN_DISTURBANCE_WINDOW=$OPTARG
      ;;
    \?)
      # getopts issues an error message
      echo -e $USAGE
      echo ""
      print_options
      echo ""
      exit 1
      ;;
  esac
done


cat <<EOF
-----------------------------------------
-- Disturbance Model
--
-- MIT License
-- 
-- Copyright (c) 2016  Federico Mari <mari@di.uniroma1.it>
--                     Toni Mancini <tmancini@di.uniroma1.it>
--                     Annalisa Massini <massini@di.uniroma1.it>
--                     Igor Melatti <melatti@di.uniroma1.it>
--                     Enrico Tronci <tronci@di.uniroma1.it>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-----------------------------------------

-- DISTURBANCES can be:
-- 0. NOP (0): NULL disturbance is performed
-- 1. FAULTS (disturbances that are recovered after REMOVE_FAULT_TIME seconds)
-- 2. INPUT VARIATION (disturbances that are not recovered)

-- FAULTS disappear after exactly REMOVE_FAULT_TIME seconds
-- Each disturbance makes time elapsing of 1 tick

---------------------------------
-- Constant declarations
---------------------------------
Const
	-- If TICKS_PER_SECOND=2 then discretization time is 0.5 secs
    TICKS_PER_SECOND : $TICKS_PER_SECOND;

    -- Time (seconds) after which disturbance is removed
    REMOVE_FAULT_TIME : $REMOVE_FAULT_TIME;

    -- Constraints on Faults
    -- Number of possible faults
    NUM_FAULTS : $NUM_FAULTS;
    ---- Maximum number of faults within a trace
    MAX_NUM_OF_FAULTS : $MAX_NUM_OF_FAULTS;
    ---- Maximum number of simultaneously active faults
    MAX_NUM_OF_ACTIVE_FAULTS : $MAX_NUM_OF_ACTIVE_FAULTS;

    -- Constraints on Inputs variations
    -- Number of possible inputs variations
    NUM_INPUTS : $NUM_INPUTS;
    ---- Maximum number of input variations within a trace
    MAX_NUM_OF_INPUTS : $MAX_NUM_OF_INPUTS;
    ---- Inject inputs at times multiple of INJECT_INPUT_PERIOD seconds
    INJECT_INPUT_PERIOD : $INJECT_INPUT_PERIOD;

    -- Disturbance window
    BEGIN_DISTURBANCE_WINDOW : $BEGIN_DISTURBANCE_WINDOW;


-----------------------------------------------
-- Type declarations
-----------------------------------------------
Type
    FAULT_TYPE : 1 .. NUM_FAULTS;
    FAULT_TYPE_WITH_NULL : 0 .. NUM_FAULTS;
EOF

if [[ $NUM_INPUTS > 0 ]]; then
	echo "    INPUT_TYPE : 1 .. NUM_INPUTS;"
fi

cat <<EOF


-----------------------------------------------
-- State var declarations
-----------------------------------------------
Var
    -- no fault == {forall i in FAULT_TYPE . time_since_last_fault[i] = -1}
    time_since_last_fault : array [ FAULT_TYPE ] of -1 .. REMOVE_FAULT_TIME*TICKS_PER_SECOND;
    number_of_faults : 0 .. MAX_NUM_OF_FAULTS;
    number_of_inputs : 0 .. MAX_NUM_OF_INPUTS;
    global_time : real(4, 99);

-----------------------------------------------
-- External procedures and functions
-----------------------------------------------

procedure init();
begin
    global_time := 0.0;
    for i : FAULT_TYPE do
        time_since_last_fault[i] := -1;  -- no fault
    endfor;
    number_of_faults := 0;
    number_of_inputs := 0;
end; -- init()

-- returns true if there are no active faults
function no_faults() : boolean;
var res : boolean; i : 1 .. NUM_FAULTS+1;
begin
    res := true;
    i := 1;
    while (i <= NUM_FAULTS & res) do
        if (time_since_last_fault[i] > -1) then
            res := false;
        endif;
        i := i + 1;
    endwhile;
    return res;
end; -- no_faults()

-- returns the number of currently active faults
function number_of_active_faults() : FAULT_TYPE_WITH_NULL;
var res : FAULT_TYPE_WITH_NULL; 
begin
    res := 0;
    for i : FAULT_TYPE do
        -- check if the i-th disturbance is active
        if (time_since_last_fault[i] > -1) then
            res := res + 1;
        endif;
    endfor;
    return res;
end; -- number_of_active_faults()

-- returns true if no disturbance needs to be removed
function no_fault_needs_to_be_removed() : boolean;
var res : boolean; i : 1 .. NUM_FAULTS+1;
begin
    res := true;
    i := 1;
    while (i <= NUM_FAULTS & res) do
        if (time_since_last_fault[i] = REMOVE_FAULT_TIME*TICKS_PER_SECOND) then
            res := false;
        endif;
        i := i + 1;
    endwhile;
    return res;
end; -- no_fault_needs_to_be_removed()

-- returns true if an input variation is allowed at this time
function is_input_variation_allowed() : boolean;
begin
	return ((floor(global_time / (INJECT_INPUT_PERIOD * TICKS_PER_SECOND)) * (INJECT_INPUT_PERIOD * TICKS_PER_SECOND)) = global_time);
end; -- no_fault_needs_to_be_removed()

procedure time_step();
begin
    global_time := global_time + 1.0;
    for i : FAULT_TYPE do
        if (time_since_last_fault[i] >= 0) then
            time_since_last_fault[i] := time_since_last_fault[i] + 1;
        endif;
    endfor;
end; -- time_step()


-----------------------------------------------
-- startstate
-----------------------------------------------

startstate "Initstate"
begin
    init(); 
end;


-----------------------------------------------
-- rules
-----------------------------------------------

-- DISTURBANCES

-- time_since_last_fault[d] = -1 means "d is not yet injected"

Ruleset d : FAULT_TYPE do
    Rule "Inject Fault"
        global_time >= BEGIN_DISTURBANCE_WINDOW &
        time_since_last_fault[d] = -1 &
        no_fault_needs_to_be_removed() &
        number_of_faults < MAX_NUM_OF_FAULTS &
        number_of_active_faults() < MAX_NUM_OF_ACTIVE_FAULTS
    ==>
    begin
        time_since_last_fault[d] := 0;
        number_of_faults := number_of_faults + 1;
        time_step();
    end;

    -- d is injected and needs to be removed (must!)
    Rule "Remove Fault"
        time_since_last_fault[d] = REMOVE_FAULT_TIME*TICKS_PER_SECOND
    ==>
    begin
        time_since_last_fault[d] := -1;  -- remove injection d
        time_step();
    end;
End;
EOF

if [[ $NUM_INPUTS > 0 ]]; then
cat <<EOF

Ruleset d : INPUT_TYPE do
    Rule "Inject Input Variation"
        global_time >= BEGIN_DISTURBANCE_WINDOW &
        no_fault_needs_to_be_removed() &
        number_of_inputs < MAX_NUM_OF_INPUTS &
        is_input_variation_allowed()
    ==>
    begin
        number_of_inputs := number_of_inputs + 1;
        time_step();
    end;
End;

EOF
fi

cat <<EOF

-- no operation is performed
Rule "NOP"
   no_fault_needs_to_be_removed()
==>
begin
    time_step();
end;


-----------------------------------------------
-- Invariants (Safety properties)
-----------------------------------------------
Invariant
    number_of_faults <= MAX_NUM_OF_FAULTS & number_of_inputs <= MAX_NUM_OF_INPUTS;

EOF
